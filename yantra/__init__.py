#!/usr/bin/python
# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of Yantra: A lattice Boltzmann method based tool for multiscale/
#multiphyics simulations
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#=======================================================================================
__author__ = 'Ravi A. Patel'   
__email__ = 'ravee.a.patel@gmail.com'
__license__ = 'GPL V3 and later'
from .version import version as __version__
#import domain,physics
from domain import *
from physics import *
from _pyevtk import hl as _hl
from _pyevtk.vtk import VtkGroup
#import _base
import importlib
import cPickle as pickle
import scipy as _sp
import numpy as _np

def load(fname):
    """
    loads a model from a file
    
    Parameters
    ----------
    fname: str
        
    """
    d=pickle.load(open( fname, 'rb')) 
    return loaddict(d)

def save(m,fname):
    d= savedict(m)
    pickle.dump(d, open( fname+'.ymp', 'wb')) 

def loaddict(d):
    """
    loads an yantra's model instance from dictionary
    
    parameters
    ----------
    d: dict
        dict generated using savedict method
    """
    signlist = d['signature'].split('.')
    sign = '.'.join(signlist[:-1])
    m=importlib.import_module(sign)
    m = getattr(m,signlist[-1])
    return m.loaddict(d)

def savedict(m):
    """
    loads an yantra's model instance from dictionary
    
    parameters
    ----------
    d: dict
        dict generated using savedict method
    
    Returns
    -------
    dict
        dictonary contain all information about m to be reloaded
    """
    return m.savedict()    

def export_to_matlab(m,fname,varname):
    """
    saves variables to have access in matlab
    
    Parameters
    ----------
    
    """
    mdict={}
    if m.d == 2:
        mdict['x'],mdict['y']= m.meshgrid()
    elif m.d == 3:
        mdict['x'],mdict['y'],mdict['z']= m.meshgrid()
    for name in varname:
        mdict[name]=getattr(m,name)
    _sp.io.savemat(fname,mdict,long_field_names=True)

def export_to_vtk(m,fname,varname):
    """
    """
    mdict={}
    if m.d == 2:
        raise ValueError("Only 3D models accepted")
    elif m.d == 3:
        x,y,z= m.meshgrid()
    for name in varname:
        val = getattr(m,name)
        if len(val.shape) > m.d: val = tuple(val)
        mdict[name]=val
        _hl.gridToVTK(fname,x,y,z,pointData=mdict)

def dict_to_vtk(fname,x,y,z,vardict):
    """
    """
    _hl.gridToVTK(fname,x,y,z,pointData=vardict)

