#!/usr/bin/python
# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of Yantra: A lattice Boltzmann method based tool for multiscale/
#multiphyics simulations
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#=======================================================================================
import os as _os
def _get_mlist():
    mlist = []
    for f in _os.listdir("."):
        if ('.' in f) and ('make' not in f) and (not f.startswith('_')):
            m = f.split('.')[0]
            if m not in mlist: mlist.append(m)
    return mlist

if __name__ == '__main__':
    _mlist = _get_mlist()
else:
    _mlist = ['ade2d', 'ade3d', 'diff2d', 'diff3d', 
             'multilevel_ade2d', 'multilevel_ade3d', 
             'multilevel_diff2d', 'multilevel_diff3d',
             'update2d', 'update3d','ns2d','ns3d']
__all__ = _mlist
for _m in _mlist:
     __import__(_m, globals(), locals(), [], -1)