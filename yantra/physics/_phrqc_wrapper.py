#!/usr/bin/python
# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of Yantra: A lattice Boltzmann method based tool for multiscale/
#multiphyics simulations
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE. See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#This a wrapper to couple phreeqc with any transport code
#
#=======================================================================================
from __future__ import division,print_function
import numpy as np
import warnings
import sys
from copy import deepcopy  
#import multiprocessing as mp
if sys.version_info > (3,0):
    range = range
else:
    range = xrange
        
try:
   import IPhreeqcPy
except ImportError:
	warnings.warn("IPhreeqcPy not installed. multicomponent reactive transport modules will not work.")

def setvar(attr):
    """
    method to set property
    """
    def set_var(self,val):
        if self._vars[attr[1:]].type=='dict':
            try:
                oldval=getattr(self,attr)
                oldval.update(val)
                setattr(self,attr,oldval)
            except AttributeError:
                v = deepcopy(self._vars[attr[1:]].default)
                v.update(val)
                setattr(self,attr,v)
        elif self._vars[attr[1:]].type=='scalar':
            ones = np.ones(self.array_shape)
            val = val*ones
            setattr(self,attr,val.flatten(order='C'))
        else:
            setattr(self,attr,val)
    return set_var

def getvar(attr):
    """
    method to get property
    """
    def get_var(self):
        if self._vars[attr[1:]].type=='scalar':
            return getattr(self,attr).reshape(self.array_shape, order='C')
        else:
            return getattr(self,attr)
    return get_var

def delvar(attr):
    """
    method to delete property
    """
    def del_var(self):
        delattr(self,attr)
        return
    return del_var

        
class Variable:
    def __init__(self,**kwargs):
        for k,v in kwargs.iteritems():
            setattr(self,k,v)
            
class PhrqcMeta(type):
    def __init__(cls, name, bases, dct):
        """
        meta class of the LB created to initalize physical variable as property
        """
        for k,v in cls._vars.iteritems():
            setattr(cls, k,property(fset=setvar("_"+k), fget=getvar("_"+k),
                             fdel=delvar("_"+k)))
            
class ParallelPhrqc():
    """
    parallel wrapper that couples phreeqc with transport codes
    """
    pass
    
class Phrqc(object):

    """
    wrapper to couple phreeqc with transport codes
    """
    __metaclass__= PhrqcMeta
    _vars={'database':Variable(default='phreeqc.dat',type='str',loc='domain_params',
                               doc='path for database'),
           'phrqc_input_file':Variable(default='',type='str',loc='domain_params',
                                doc='path for phreeqc input file'),
           'startcell':Variable(default=0,type='int',loc='solver_params',
                                doc='number of start cell'),
           'stopcell':Variable(default=0,type='int',loc='solver_params',
                               doc='number of end cell'),
           'solution_labels':Variable(default=0,type='scalar',loc='domain_params',
                                      doc='array of solution lable cells are assigned'),
           'poros':Variable(default=1,type='scalar',loc='domain_params',
                                      doc='water filled porosity'),
           'boundary_solution_labels':Variable(default={},type='dict',
                                                loc='boundary_params',doc='solution labels for boundary'),
           'eq_names':Variable(default=[],type='list',loc='solver_params',
                                    doc='list of names of equilibrium phases'),
           'ss_names':Variable(default={},type='dict',loc='solver_params',
            doc='dictonary listing solid solution name as keys and each keys corresponds to link of components in solid solution'),
           'kin_names':Variable(default=[],type='list',loc='solver_params',
                                    doc='list of names kinetic phases'),
           'tracer_components':Variable(default=[],type='list',loc='solver_params',
                                        doc='list of tracer components'),
           'O_norm':Variable(default=55.506216797268586,type='float',
                             doc='number of moles of O in water'),
           'H_norm':Variable(default=111.01243359454628,type='float',loc='solver_params',
                             doc='number of moles of H in water'),
           'extra_selected_out_punch_head':Variable(default=[],type='list',loc='solver_params',
                                                    doc='headings for extra punch statements in selected output'),
           'extra_selected_out_punch_statements':Variable(default=[],type='list',loc='solver_params',
                                                          doc = 'punch statements for selected output'),
           'time':Variable(default=0,type='float',loc='solver_params',doc='time till simulation'),
           'dt':Variable(default=0,type='float',loc='solver_params',doc='timestep'),
           'iters':Variable(default=0,type='int',loc='solver_params',doc='number of iterations'),
            'phrqc_smart_run_tol':Variable(default=1e-6,type='float',loc='solver_params',doc='tolerance for smart run'),
           'phrqc_flags':Variable(default={'only_interface':False,
                                           'only_fluid':False,
                                           'smart_run':False,
                                           'include_OH':True,
                                           'update_output':True
                                            },type='dict',loc='solver_params', 
                                            skip_initalization=True,
                                            doc='flags for phrqc wrapper'),
         'prev_selected_output':Variable(default={},type='dict',loc='domain_params',
                                    doc='private variable for phreeqc selected output for previous timestep'),
         'ss':Variable(default={},type='dict',loc='domain_params',
                                    doc='source sink term for transport step'),
        }

    
    def __init__(self, domain,domain_params,bc_params,solver_params):
        """
        Initialize phreeqc model

        Arguments

        """
        self._selected_output ={}
        if domain.d == 2:
            self.array_shape= (domain.ny,domain.nx)
        elif domain.d == 3:
            self.array_shape = (domain.nz,domain.ny,domain.nx)
        else:
            raise ValueError('dimension of the domain not understood')
        inputs = deepcopy(domain_params)
        inputs.update(solver_params)
        if 'solution_labels' in bc_params:
            inputs['boundary_solution_labels']= bc_params['solution_labels']
        for k,v in self._vars.iteritems():
            setattr(self,k,inputs.get(k,v.default))
        self.ncells= len(self._solution_labels)
        try:
            assert self.ncells == (self.stopcell-self.startcell)+1
        except AssertionError:
            self.stopcell = self.ncells
            self.startcell = 1
        self.IPhreeqc = IPhreeqcPy.IPhreeqc()
        self.IPhreeqc.LoadDatabase(self.database)
        self.IPhreeqc.RunFile(self.phrqc_input_file)
        self.IPhreeqc.RunString(self.phrqc_input)
        self.phrqc_flags['update_output']=True
        self.O_norm = self.selected_output()['H2O'][0]
        self.H_norm = 2.0 * self.selected_output()['H2O'][0]

    @property
    def phrqc_input(self):
        """
        input for phreeqc includes selected output string
        """
        with open(self.phrqc_input_file,'rb') as f:
            string = f.read()
        string +='\n'+self.selected_output_str
        string +='\nend'
        return string
    
    @property
    def selected_output_str(self):
        """
        Generates selected output string
        """
        selected_output= []
        selected_output.append('SELECTED_OUTPUT')
        selected_output.append('\t-reset false')
        selected_output.append('\t-time false')
        selected_output.append('\t-high_precision true')
        selected_output.append('\t-solution true')
        selected_output.append('\t-pH true')
        selected_output.append('\t-pe false')
        selected_output.append('\t-charge_balance false')
        selected_output.append('\t-alkalinity true')
        selected_output.append('\t-ionic_strength true')
        selected_output.append('\t-percent_error false')
        selected_output.append('USER_PUNCH')
        user_punch_head =[]
        user_punch_str = []
        counter = 0
        user_punch_head.append("H2O")
        counter+=10
        user_punch_str.append(str(counter)+'\tpunch'+'\tmol("H2O")')
        user_punch_head.append("poros")
        counter+=10
        user_punch_str.append(str(counter)+'\tpunch'+'\ttot("water")')
        for name in self.components:
            user_punch_head.append(name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\ttot("%s")'%name)
        for name in self.eq_names:
            user_punch_head.append(name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\tequi("%s")'%name)
            user_punch_head.append('SI_'+name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\tSI("%s")'%name)
        for name in self.kin_names:
            user_punch_head.append(name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\tequi("%s")'%name)
            user_punch_head.append('SI_'+name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\tSI("%s")'%name)
        ss_component_names = []
        for comp in self.ss_names.values():
            ss_component_names.extend(comp)
        for name in ss_component_names:
            user_punch_head.append(name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\ts_s("%s")'%name)
            user_punch_head.append('SI_'+name)
            counter+=10
            user_punch_str.append(str(counter)+'\tpunch'+'\tSI("%s")'%name)
        user_punch_head.extend(self.extra_selected_out_punch_head)
        counter+=10
        extra_statements=str(counter)+'\tpunch'+'\t'.join(self.extra_selected_out_punch_statements)
        user_punch_str.append(extra_statements)
        selected_output.append('\t-headings %s'%(' '.join(user_punch_head)))
        selected_output.append('\t-start')
        selected_output.append('\n\t\t'.join(user_punch_str))
        selected_output.append('\t-end')
        return '\n'.join(selected_output)
    
    @property 
    def components(self):
        """
        list of components in phreeqc
        """
        try:
            getattr(self,'_components')
        except AttributeError:
            components= self.IPhreeqc.GetComponentList()
            if u'H' not in components:
                components.append(u'H')
            if u'O' not in components:
                components.append(u'O')
            if not self.phrqc_flags['include_OH']:
                components.remove(u'H')
                components.remove(u'O')
            self._components=components
        return self._components
    
    @property
    def active_components(self):
        """
        lists only non tracer components
        """
        return [name for name in self.components
                                if name not in self.tracer_components]
    
    @property    
    def  boundary_conditions(self):
        """
        gets boundary condition for transport model
        """
        if self.time == 0:
            results = self.selected_output()
            self._bc_solutions = {}
            for bc,label in self.boundary_solution_labels.iteritems():
                self._bc_solutions[bc]={}
                idx = results[u'soln'].index(label)
                for  name in self.components:
                    self._bc_solutions[bc][name]=results[name][idx]
        return self._bc_solutions
   
    @property
    def initial_conditions(self):
        """
        gets initial conditions for transport model
        """
        if self.time == 0:
            copycellstr = []
            for (cell, label) in zip(range(self.startcell,self.stopcell+1,1),
                 self._solution_labels):
                copycellstr.append('copy cell %i %i' % (label, cell))
            copycellstr = '\n'.join(copycellstr)
            runcellstr=['RUN_CELLS']
            runcellstr.append('\t-cells %i-%i'%(self.startcell,self.stopcell))
            runcellstr.append('\t-start_time 0')
            runcellstr.append('\t-time_step 0')
            runcellstr = '\n'.join(runcellstr)
            self.IPhreeqc.AccumulateLine(copycellstr)
            self.IPhreeqc.AccumulateLine('end')
            self.IPhreeqc.AccumulateLine(runcellstr)
            self.IPhreeqc.AccumulateLine('end')
            self.IPhreeqc.RunAccumulated()
            self.phrqc_flags['update_output'] = True
            self.selected_output(merge_with_previous=False,toarray=True,reshape=True)
            self.poros = self.selected_output()['poros']
            self._initial_conditions=deepcopy(self.component_conc)
        return self._initial_conditions
                
    def modify_solution(self,c,dt,nodetype):
        """
        inconc/prevconc flatten dictonary of conc subset.
        nodetype/solid_phase_qty:matrix or flattened either is possible
        to be run by current iphreeqc solver.

        input:
        -----
        inconc = dictionary  containing each component as key and ncells \
        values of conc for each key
        current_time=current time in sec
        nodetype= node type matrix

        output:
        -------
        updates currentselecout
        """
        self.dt = dt
        active_nodes = self.active_nodes(c,nodetype)
        c_trans=deepcopy(c)
        moles = self.flatten_dict(self.to_moles(c))
        modify_str=[]
        runcells=[]
        runcell_str=[]
        for i,cell in enumerate(range(self.startcell,self.stopcell+1,1)):
            if active_nodes[i]:
                runcells.append(str(cell))
                modify_str.append('SOLUTION_MODIFY %i' % cell)
                if 'H' in moles:
                    c = moles['H'][i]
                    if c <=0: c= 1e-30
                    modify_str.append('\t%s\t%.20e' % ('total_h', c))
                if 'O' in moles:
                    c = moles['O'][i]
                    if c <=0: c= 1e-30
                    modify_str.append('\t%s\t%.20e' % ('total_o', c))
                modify_str.append('\t-totals')
                for name,val in moles.iteritems():
                    if (name != 'H') and (name!='O'):
                        c = val[i]
                        if c <=0: c= 1e-30
                        modify_str.append('\t\t%s\t%.20e' % (name, c))
        modify_str.append('end')
        modify_str ='\n'.join(modify_str)
        runcell_str.append('RUN_CELLS')
        runcell_str.append('\t-cells %s'%'\n\t\t'.join(runcells))
        runcell_str.append('\t-start_time %s'%self.time)
        runcell_str.append('\t-time_step %s'%self.dt)
        runcell_str.append('end')
        runcell_str='\n'.join(runcell_str)
        self.IPhreeqc.AccumulateLine(modify_str)
        self.IPhreeqc.AccumulateLine(runcell_str)
        self.IPhreeqc.RunAccumulated()
#        c_prev= deepcopy(self.component_conc)
        self.phrqc_flags['update_output'] = True
        self.selected_output(merge_with_previous=True,toarray=True,reshape=True)
        self.poros = self.selected_output()['poros']
        c_current = self.component_conc
        ss={}
        for name in self.components:
            ss[name] = (c_current[name]-c_trans[name])/self.dt
            ss[name] = ss[name] *(active_nodes.reshape(self.array_shape)>0)
            ss[name] *= self.poros
        self.time+=self.dt
        self.iters+=1
        return ss

    def active_nodes(self,c,nodetype):
        """
        gives active nodes for phreeqc
        """
        active =np.ones(self.array_shape).flatten()
        prev_c = self.component_conc
        smart_inactive = np.zeros(self.array_shape)
        inactive=0
        if self.iters >1 and self.phrqc_flags['smart_run']:
            for name in self.active_components:
                diff =np.abs( c[name]*(c[name]-prev_c[name])/(c[name]+1e-30)**2)
                smart_inactive +=1*(diff<self.phrqc_smart_run_tol)
        inactive+=1*(smart_inactive.flatten(order='c') >0)
        if self.phrqc_flags['only_interface']:
            inactive += (1*(nodetype!=0)-1*(nodetype==0)).flatten()
        if self.phrqc_flags['only_fluid']:
            inactive += (1*(nodetype>0)-1*(nodetype<=0))
        tot_solid_phase_conc = self.add_dict(self.flatten_dict(self.solid_phase_conc))
        inactive += -1*(tot_solid_phase_conc>0)
        active -= 1*(inactive>0)
        self.nactive = np.sum(active)
        return active
            
    @property    
    def component_conc(self):
        """
        concentration of aqueous components 
        """
        selected_output = self.selected_output()
        output={}
        for name in self.components:
            output[name]=selected_output[name]
        return output
    
    @property
    def solid_phase_conc(self):
        """
        concentration of solid phases
        """
        selected_output = self.selected_output()
        phases={}
        for name in self.eq_names:
            phases[name]= selected_output[name]
        for name in self.kin_names:
            phases[name]= selected_output[name]
        for components in self.ss_names.itervalues():
            for component in components:
                phases[component]= selected_output[component]
        return phases
                      
    def modify_eq(self, phaseqty):
        """
        modifies the phaseqty in phreeqc
        
        Parameters
        ----------
        phaseqty: dict
            dictionary containing an ndarray of new quantities of equilibrium phases
        """
        phaseqty = self.flatten_dict(phaseqty)
        modifystr = []
        for cell in range(self.startcell,self.stopcell+1,1):
            modifystr.append("EQUILIBRIUM_PHASES_MODIFY %d" % cell)
            for key in phaseqty.keys():
                modifystr.append("\t -component %s" %(key))
                modifystr.append("\t\t%s\t%.20e" %('-moles', phaseqty[key][cell-1]))
        modifystr.append("end")       
        modifystr ='\n'.join(modifystr)
        self.IPhreeqc.RunString(modifystr)

    def modify_kin(self, kinqty):
        """
        modifies the kinetic phases in phreeqc
        
        Parameters
        ----------
        kinqty: dict
            dictionary containing an ndarray of new quantities of kinetic phases
        
        """
        kinqty = self.flatten_dict(kinqty)
        modifystr = []
        for cell in range(self.startcell,self.stopcell+1,1):
            modifystr.append("KINETICS_MODIFY %d" % (cell))
            for key in kinqty.keys():
                modifystr.append("\t -component %s" %key)
                modifystr.append("\t\t%s\t%.20e" %('-m', kinqty[key][cell-1]))
                modifystr.append("\t\t%s\t%.20e" %('-m0', kinqty[key][cell-1]))
        modifystr.append("end")       
        modifystr ='\n'.join(modifystr)
        self.IPhreeqc.RunString(modifystr)
             
    def modify_ss(self,ssqty):
        """
        modify solid solution in phreeqc
        
        Parameters
        ----------
        ssqty: dict
            dictionary containing an ndarray of new quantities of kinetic phases
        """
        ssqty = self.flatten_dict(ssqty)
        modifystr = []
        for cell in range(self.startcell,self.stopcell+1,1):
            modifystr.append("SOLID_SOLUTIONS_MODIFY %d" % (cell))
            for key in ssqty.keys():
                if key in self.ss_names:
                    modifystr.append("\t-solid_solution %s"%key)
                    for comp in ssqty[key].keys():
                        modifystr.append("\t-component %s"%comp)
                        modifystr.append("\t\t%s\t%.20e" %('-moles',ssqty[key][comp][cell-1]))
        modifystr.append("end")       
        modifystr ='\n'.join(modifystr)
        self.IPhreeqc.RunString(modifystr)
        
    def to_moles(self, c):
        """
        converts concentration to moles which can be used in modify_solution method
        
        Parameters
        ----------
        c: dict
            dictonary containing ndarray of cocentrations for different components 
        """
        moles = self.flatten_dict(c)
        for name in self.components:
            if name == 'H':
                moles[name] += self.H_norm
                moles[name] *= self._poros
            elif name == 'O':
                moles[name] += self.O_norm
                moles[name] *= self._poros
            else:
                moles[name] *= self._poros
        return moles

    def selected_output(self,merge_with_previous=False,toarray=False,reshape=False):
        """
        returns selected output. The new selected output is generated if phrqc_flags['update_output'] \
        is true else returns the currently saved output
        
        Parameters
        ----------
        merge_with_previous: boolean
            if true merges the current selected output with previously saved output
        
        toarray: boolean
            if true converts output into array
        
        reshape: boolean
            if true resphases arrays to the array_shape
        """
        if self.phrqc_flags['update_output']:
            self.prev_selected_output = deepcopy(self._selected_output)
            output=self.IPhreeqc.GetSelectedOutputArray()
            selected_output={}
            if len(output) > 0:
                header = output[0]
                for head in header:
                     selected_output[head] = []
                for row in output[1:]:
                    for (i, head) in enumerate(header):
                        results = row[i]
                        if head == 'H':
                            results -= self.H_norm
                        elif head == 'O':
                            results -= self.O_norm
                        selected_output[head].append(results)
            if merge_with_previous: 
                merged_output = self.list_dict(self.flatten_dict(self.prev_selected_output))
                for name,val in selected_output.iteritems():
                    for i,cell in enumerate(selected_output[u'soln']): 
                        merged_output[name][cell-1]=val[i]             
                selected_output = merged_output
#            convert to ndarray
            if toarray:
                selected_output = self.ndarray_dict(selected_output)
                if reshape:
                    selected_output = self.reshape_dict(selected_output,self.array_shape)
            self._selected_output = selected_output
            self.phrqc_flags['update_output'] = False
        return self._selected_output
    
    @property
    def dphases(self):
        """
        difference of phases in a given timestep after reaction
        """
        if self.time >0:
            output={}
            current_selected_out = self.selected_output()
            prev_selected_out = self.prev_selected_output
        for name in self.eq_names:
            output[name] = current_selected_out[name] - prev_selected_out[name]
        for name in self.kin_names:
            output[name] = current_selected_out[name] - prev_selected_out[name]
        for ss in self.ss_names.keys():
            for name in self.ss_names[ss]:
                output[name] = current_selected_out[name] - prev_selected_out[name]
        return output
        
    def sort_phases(self,phaseqty):
        """
        sorts phases in equillibrium phases, solid solutions and kinetic phases
        
        Parameters
        ----------
        phaseqty: dict
            dictonary of ndarray giving quantities of phases
            
        Returns
        -------
        (dict, dict, dict)
            dictonary of eqillibrium phases, solid solutions and kinetic phases
        """
        eqphases = {}
        for name in self.eq_names:
            if name in phaseqty:
                eqphases[name] = phaseqty[name]
        ssphases = {}
        for name,components in self.ss_names.iteritems():
            ssphases[name] ={}
            for component in components: 
                if component in phaseqty:
                    ssphases[name][component] = phaseqty[component]
        kinphases = {}
        for name in self.kin_names:
            if name in phaseqty:
                kinphases[name] = phaseqty[name] 
        return eqphases,ssphases,kinphases
                
    def modify_solid_phases(self,phaseqty):
        """
        modifies solid phases to the qunatities given as input in phaseqty
        
        Parameters
        ----------
        phaseqty: dict
            dictonary of ndarray giving quantities of solid phases
        """
        #first sort the phases
        for name,val in phaseqty.iteritems():
            self._selected_output[name] = val
        eqphases, ssphases, kinphases = self.sort_phases(phaseqty)
        if len(eqphases)>0:
            self.modify_eq(eqphases)
        if len(ssphases)>0:
            self.modify_ss(ssphases)            
        if len(kinphases)>0:
            self.modify_kin(kinphases)     
#        runcellstr=['RUN_CELLS']
#        runcellstr.append('\t-cells %i-%i'%(self.startcell,self.stopcell))
#        runcellstr='\n'.join(runcellstr)
#        self.IPhreeqc.AccumulateLine(runcellstr)
#        self.IPhreeqc.AccumulateLine('end')
#        self.IPhreeqc.RunAccumulated()
#        self.phrqc_flags['update_output'] = True
#        self.selected_output(merge_with_previous=False,toarray=True,reshape=True)

    def flatten_dict(self,d):
        """
        flattens ndarray in the dictionary
        
        Parameters
        ----------
        d: dict
            input dictionary
        """
        new_d = deepcopy(d)
        for k,v in new_d.iteritems():
            if type(v).__name__ =='ndarray':
                new_d[k]=v.flatten(order='C')
            elif type(v).__name__=='dict':
                new_d[k]=self.flatten_dict(v)
        return new_d
 
    def reshape_dict(self,d,shape):
        """
        reshapes ndarray in the dictionary
        
        Parameters
        ----------
        d: dict
            input dictionary
        """
        new_d = deepcopy(d)
        for k,v in new_d.iteritems():
            if type(v).__name__ =='ndarray':
                new_d[k]=v.reshape(shape, order='C')
            elif type(v).__name__=='dict':
                new_d[k]=self.reshape_dict(v)
        return new_d
 
    @staticmethod
    def ndarray_dict(d):
        """
        convert list  dictionary into ndarray dictionary
        
        Parameters
        ----------
        d: dict
            input dictionary
        """
        new_d = deepcopy(d)
        for k,v in new_d.iteritems():
            new_d[k]=np.array(v)
        return new_d

    @staticmethod
    def list_dict(d):
        """
        convert ndarray  dictionary into list dictionary
        
        Parameters
        ----------
        d: dict
            input dictionary
        """
        new_d = deepcopy(d)
        for k,v in new_d.iteritems():
            new_d[k]=list(v)
        return new_d
    
    @staticmethod
    def add_dict(d):
        tot=0
        for v in d.itervalues():
            tot += v
        return tot